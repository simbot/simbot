/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#ifndef STRAYICON_H
#define STRAYICON_H

#include <QtWidgets/QAction>
#include <QtWidgets/QMenu>
#include <QtWidgets/QSystemTrayIcon>

class sControl;

class sTrayIcon : public QSystemTrayIcon
{
	Q_OBJECT

public:
	sTrayIcon( sControl *control );
	~sTrayIcon();

public slots:
	void activated( QSystemTrayIcon::ActivationReason r );

private:
	sControl *m_control;
};

#endif // STRAYICON_H
