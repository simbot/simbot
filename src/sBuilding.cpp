/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#include <QtCore/QJsonArray>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonValue>
#include <QtCore/QStringList>
#include <QtNetwork/QNetworkReply>

#include "sAccount.h"
#include "sBuilding.h"

sBuilding::sBuilding( sAccount *account, sNetwork *network )
{
	m_account = account;
	m_network = network;
}

sBuilding::~sBuilding()
{
}

binfo_t sBuilding::info( int id, int x, int y )
{
	QNetworkReply *reply = m_network->request( QString( "http://simcracy.com/region/%1:%2" ).arg( QString::number( x ), QString::number( y ) ) );
	QString data = reply->readAll();
	reply->deleteLater();

	int appTime = m_network->getData( data, "App.TIME=([^;]+);" ).toInt();
	data = m_network->getData( data, "Region.data.Buildings=([^;]+);" );

	QJsonDocument doc = QJsonDocument::fromJson( data.toUtf8() );
	QJsonArray arr = doc.object().value( QString::number( id ) ).toArray();
	QJsonObject obj = QJsonDocument::fromJson( arr[ 9 ].toString().toUtf8() ).object();

	binfo_t info;
	info.capacity = obj.value( "w" ).toVariant().toDouble();
	info.capacity_max = obj.value( "wm" ).toVariant().toDouble();
	info.work_time = obj.value( "wt" ).toVariant().toDouble();
	info.product = obj.value( "pi" ).toVariant().toInt();
	info.lvl = ( int )arr[ 6 ].toDouble();
	info.pay = arr[ 8 ].toDouble();
	
	//QByteArray aData;
	//aData.append( QString( "x=%1&" ).arg( QString::number( x ) ) );
	//aData.append( QString( "y=%1" ).arg( QString::number( y ) ) );

	//reply = m_network->request( "http://simcracy.com/ajax/region/info", sNetwork::RQ_AJAX, aData );
	//data = reply->readAll();
	//reply->deleteLater();

	//obj = QJsonDocument::fromJson( data.toUtf8() ).object().value( "data" ).toObject().value( "bonuses" ).toObject();
	//if ( obj.value( QString::number( info.product ) ).isUndefined() )
		info.bonus = 0;
	//else
	//	info.bonus = obj.value( QString::number( info.product ) ).toString().toInt();

	int capacity = ( int )( ( ( appTime - info.work_time ) / ( 60 * 60 * 24 ) ) * info.capacity_max + info.capacity );
	if ( info.capacity_max < capacity )
		capacity = info.capacity_max;

	int skill = ( int )( sqrt( m_account->skill( info.product ) ) );
	skill = skill / 10 + 1;
	double mult = 1 + ( info.lvl - 2 ) / 20 + info.bonus / 100 + skill / 10;

	if ( capacity > m_account->energy().current * mult )
		capacity = ( int )( m_account->energy().current * mult );

	info.capacity = capacity;

	return info;
}

void sBuilding::work( int id, int count )
{
	QByteArray data;
	data.append( QString( "bid=%1&" ).arg( QString::number( id ) ) );
	data.append( QString( "am=%1" ).arg( QString::number( count ) ) );

	QNetworkReply *reply = m_network->request( "http://simcracy.com/ajax/building/work", sNetwork::RQ_AJAX, data );

	m_account->updateFromAjax( reply->readAll() );

	reply->deleteLater();
}

void sBuilding::workMax( int id, int x, int y )
{
	binfo_t infos = info( id, x, y );
	work( id, infos.capacity );
}
