/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#include <QtCore/QFile>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonValue>

#include "sBot.h"
#include "sBuilding.h"
#include "sInventory.h"

sBot::sBot( const QString &path )
{
	m_status = STATUS_LOADING;
	emit somethingChanged();

	m_path = path;

	m_log = new sLog( path );
	m_log->information( "Bot started" );

	m_timer = new QTimer();
	m_timer->setSingleShot( true );
	connect( m_timer, &QTimer::timeout, this, &sBot::runScripts );

	if ( !loadSettings() )
	{
		m_status = STATUS_LOAD_FAILED;
		emit somethingChanged();
		return;
	}

	m_account = new sAccount( m_log, m_network );
	connect( m_account, &sAccount::somethingChanged, this, &sBot::somethingChanged );
	
	initScriptEngine();

	m_status = STATUS_LOADED;
	emit somethingChanged();

	//m_timer->start();
	//runScripts();
}

sBot::~sBot()
{
	if ( m_account )
		m_account->deleteLater();
	if ( m_log )
		m_log->deleteLater();
	if ( m_network )
		m_network->deleteLater();
	if ( m_timer )
		m_timer->deleteLater();
	if ( m_script )
		m_script->deleteLater();
}

sBot::sBotStatus sBot::status()
{
	sNetwork::sNetworkStatus nStatus = m_network->status();
	if ( nStatus == sNetwork::STATUS_AUTHED )
	{
		if ( m_status == STATUS_LOADED )
			m_status = STATUS_AUTHED;
		else
			m_status = STATUS_OK;
	}
	else
	{
		m_status = STATUS_AUTH_FAILED;
	}

	return m_status;
}

QString sBot::statusText()
{
	sBotStatus status = this->status();

	switch ( status )
	{
		case STATUS_LOADING:
			return "Loading";

		case STATUS_LOADED:
			return "Loaded";

		case STATUS_AUTHED:
			return "Authed";

		case STATUS_OK:
			return "Running";

		case STATUS_LOAD_FAILED:
			return "Load failed";

		case STATUS_AUTH_FAILED:
			return "Auth failed";

		case STATUS_UNKNOWN:
		default:	
			return "Unknown";
	}
}

QNetworkAccessManager *sBot::networkAccessManager()
{
	return m_network->networkAccessManager();
}

sAccount *sBot::account()
{
	return m_account;
}

int sBot::loadSettings()
{
	QString fileName = m_path + "/settings.json";
	QFile settingsFile( fileName );
	if ( !settingsFile.open( QIODevice::ReadOnly | QIODevice::Text ) )
	{
		m_log->error( QString( "The file %1 can't be opened for reading." ).arg( fileName ) );
		return 0;
	}

	QJsonDocument settingsJson = QJsonDocument::fromJson( settingsFile.readAll() );
	settingsFile.close();

	if ( !settingsJson.isObject() )
	{
		m_log->error( QString( "The file %1 has invalid format." ).arg( fileName ) );
		return 0;
	}

	QJsonObject settings = settingsJson.object();

	int type = ( settings.value( "type" ).toString() == "google" );
	m_network = new sNetwork( settings.value( "email" ).toString(), settings.value( "password" ).toString(), type, m_log );

	if ( !settings.value( "interval" ).isNull() && settings.value( "interval" ).toVariant().toInt() > 0 )
		m_timer->setInterval( settings.value( "interval" ).toVariant().toInt() * 60 * 1000 );
	else
		m_timer->setInterval( 15 * 60 * 1000 );

	m_scripts = QDir( m_path + "/scripts" ).entryList( QStringList( "*.js" ), QDir::Files | QDir::Readable, QDir::Name );

	return 1;
}

void sBot::initScriptEngine()
{
	m_script = new sScriptEngine( m_path + "/scripts/" );

	m_script->addProperty( m_account, "account" );
	m_script->addProperty( m_account->building(), "buildings" );
	m_script->addProperty( m_account->inventory(), "inventory" );
	m_script->addProperty( m_log, "log" );	
}

void sBot::runScripts()
{
	foreach ( QString path, m_scripts )
	{
		QString scriptFilePath = m_path + "/scripts/" + path;

		m_log->information( QString( "Script %1 started." ).arg( scriptFilePath ) );
		QFile scriptFile( scriptFilePath );
		if ( !scriptFile.open( QIODevice::ReadOnly ) )
		{
			m_log->error( QString( "Can't open %1 to read." ).arg( scriptFilePath ) );
			continue;
		}

		QString code = scriptFile.readAll();
		scriptFile.close();

		QScriptSyntaxCheckResult syntaxResult = m_script->checkSyntax( code );
		if ( syntaxResult.state() != QScriptSyntaxCheckResult::Valid )
		{
			m_log->error( QString( "Syntax error in file %1 at line %2. Error message: %3" ).arg( scriptFilePath,
										QString::number( syntaxResult.errorLineNumber() ), syntaxResult.errorMessage() ) );
			continue;
		}

		m_account->updateInformation();

		m_script->pushContext();
		QScriptValue result = m_script->evaluate( code );
		m_script->popContext();
		if ( m_script->hasUncaughtException() )
		{
			m_log->error( QString( "Uncaught exception in file %1 an line %2. Error message: %3" ).arg( scriptFilePath,
										QString::number( m_script->uncaughtExceptionLineNumber() ), result.toString() ) );
		}
		else
		{
			m_log->information( QString( "Script %1 done successfully." ).arg( scriptFilePath ) );
		}
	}
}
