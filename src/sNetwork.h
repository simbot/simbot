/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#ifndef SNETWORK_H
#define SNETWORK_H

#include <QtCore/QObject>
#include <QtNetwork/QNetworkAccessManager>

#include "sLog.h"

class sNetwork : public QObject
{
	Q_OBJECT

public:
	enum
	{
		RQ_GET,
		RQ_POST,
		RQ_AJAX
	};
	
	enum sNetworkStatus
	{
		STATUS_AUTHED,
		STATUS_NOTAUTHED
	};

	sNetwork( const QString &email, const QString &password, int type, sLog *log );
	~sNetwork();

	QNetworkReply *request( const QString &url, int type = RQ_GET, QByteArray &data = QByteArray() );

	QString getData( const QString &data, const QString &pattern );
	QString getData( const QString &pattern );

	QNetworkAccessManager *networkAccessManager();

	sNetworkStatus status();

private:
	enum
	{
		SN_NONE,
		SN_NO_REDIRECT
	};

	QNetworkReply *synReq( QNetworkRequest &req, int type = 0, QByteArray &data = QByteArray() );

	void auth();
	bool isAuthorised();

	sNetworkStatus m_status;

	sLog *m_log;
	QNetworkAccessManager *m_manager;

	QString m_email;
	QString m_password;
	int m_type; // 0 - facebook, 1 - google

	QString m_token;
};

#endif // SNETWORK_H
