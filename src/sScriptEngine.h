/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#ifndef SSCRIPTENGINE_H
#define SSCRIPTENGINE_H

#include <QtCore/QString>
#include <QtScript/QScriptEngine>

class sScriptEngine : public QScriptEngine
{
	Q_OBJECT

public:
	sScriptEngine( const QString &path );
	~sScriptEngine();

	void addProperty( void *object, const QString &name );

private:
	void init();

	static QScriptValue include( QScriptContext *context, QScriptEngine *engine );
};

#endif // SSCRIPTENGINE_H
