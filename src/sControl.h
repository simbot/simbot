/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#ifndef SCONTROL_H
#define SCONTROL_H

#include <QtCore/QList>
#include <QtWidgets/QWidget>

#include "sBot.h"
#include "sTrayIcon.h"

#include "ui_sControl.h"

class sControl : public QWidget
{
	Q_OBJECT

public:
	sControl();
	~sControl();
	
private:
	void load();
	void refreshInfo( sBot *bot );

	Ui::sControlForm ui;
	sTrayIcon *m_trayIcon;

	QList< sBot * > m_bots;
};

#endif // SCONTROL_H
