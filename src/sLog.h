/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#ifndef SLOG_H
#define SLOG_H

#include <QtCore/QObject>
#include <QtCore/QDateTime>
#include <QtCore/QFile>
#include <QtCore/QString>

class sLog : public QObject
{
	Q_OBJECT

public:
	sLog( const QString &path );
	~sLog();

	Q_INVOKABLE void debug( const QString &message );
	Q_INVOKABLE void information( const QString &message );
	Q_INVOKABLE void warning( const QString &message );
	Q_INVOKABLE void error( const QString &message );
	
private:
	void openFile();
	void closeFile();

	void write( const QString &beginning, const QString &message );

	QDate m_date;
	QString m_path;

	QFile *m_logFile;
};

#endif // SLOG_H
