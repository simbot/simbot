/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#ifndef STYPES_H
#define STYPES_H

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>

struct exp_t
{
	double current;
	double left;
	double next;
};

struct cm_pair_t
{
	double current;
	double max;
};

struct binfo_t
{
	double capacity;
	double capacity_max;
	double work_time;
	double pay;
	double bonus;
	int lvl;
	int product;
};

Q_DECLARE_METATYPE( exp_t );
Q_DECLARE_METATYPE( cm_pair_t );
Q_DECLARE_METATYPE( binfo_t );

class sTypes
{
public:
	// exp_t
	static QScriptValue toScriptValue( QScriptEngine *engine, const exp_t &s );
	static void fromScriptValue( const QScriptValue &obj, exp_t &s );

	// cm_pair_t
	static QScriptValue toScriptValue( QScriptEngine *engine, const cm_pair_t &s );
	static void fromScriptValue( const QScriptValue &obj, cm_pair_t &s );
};

#endif // STYPES_H
