/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#include <QtCore/QJsonArray>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonValue>
#include <QtCore/QMap>
#include <QtCore/QStringList>

#include <QtNetwork/QNetworkReply>

#include "sAccount.h"

#include <qdebug.h>

sAccount::sAccount( sLog *log, sNetwork *network )
{
	m_log = log;
	m_network = network;

	m_building = new sBuilding( this, m_network );
	m_inventory = new sInventory( m_network );

	updateInformation();
}

sAccount::~sAccount()
{
}

int sAccount::id()
{
	return m_id;
}

int sAccount::x()
{
	return m_x;
}

int sAccount::y()
{
	return m_y;
}

int sAccount::lvl()
{
	return m_lvl;
}

double sAccount::skill( int id )
{
	return m_skills[ id ];
}

exp_t sAccount::exp()
{
	return m_exp;
}

cm_pair_t sAccount::energy()
{
	return m_energy;
}

cm_pair_t sAccount::hp()
{
	return m_hp;
}

cm_pair_t sAccount::mood()
{
	return m_mood;
}
	
double sAccount::money()
{
	return m_money;
}

sBuilding *sAccount::building()
{
	return m_building;
}

sInventory *sAccount::inventory()
{
	return m_inventory;
}

void sAccount::consume( int id, int count )
{
	QByteArray data;
	data.append( QString( "iid=%1&" ).arg( QString::number( id ) ) );
	data.append( QString( "am=%1" ).arg( QString::number( count ) ) );

	QNetworkReply *reply = m_network->request( "http://simcracy.com/ajax/user/consume", sNetwork::RQ_AJAX, data );

	updateFromAjax( reply->readAll() );

	reply->deleteLater();
}

void sAccount::sendMessage( int id, QString message )
{
	QByteArray data;
	data.append( QString( "aid=%1&" ).arg( QString::number( id ) ) );
	data.append( QString( "text=%1" ).arg( message ) );

	QNetworkReply *reply = m_network->request( "http://simcracy.com/ajax/messages/send", sNetwork::RQ_AJAX, data );
	reply->deleteLater();

	m_log->information( QString( "sent message to %1: %2" ).arg( QString::number( id ), message ) );
}

void sAccount::sendItems( int id, double money, QVariantList items )
{
	QByteArray data;
	data.append( QString( "aid=%1&" ).arg( QString::number( id ) ) );
	data.append( QString( "money=%1&" ).arg( QString::number( money ) ) );

	int i = 0;
	foreach( QVariant item, items )
	{
		QMap< QString, QVariant > pair = item.toMap();

		data.append( QString( "items[%1][id]=%2&" ).arg( QString::number( i ), pair.value( "id" ).toString() ) );
		data.append( QString( "items[%1][am]=%2&" ).arg( QString::number( i ), pair.value( "count" ).toString() ) );

		i++;
	}

	QNetworkReply *reply = m_network->request( "http://simcracy.com/ajax/account/senditems", sNetwork::RQ_AJAX, data );

	updateFromAjax( reply->readAll() );

	reply->deleteLater();

	m_log->information( QString( "sent items to %1: money %2, items: %3" ).arg( QString::number( id ), QString::number( money ) , data ) );
}

void sAccount::updateInformation()
{
	QJsonDocument data = QJsonDocument::fromJson( m_network->getData( "App.initUser\\(([^;]+)\\);" ).toUtf8() );
	QJsonObject obj = data.object();

	m_id = obj.value( "activeaid" ).toVariant().toInt();
	m_x = obj.value( "x" ).toVariant().toInt();
	m_y = obj.value( "y" ).toVariant().toInt();

	m_exp.current = obj.value( "exp" ).toVariant().toDouble();

	m_lvl = ( int )sqrt( m_exp.current / 100 ) + 1;

	m_exp.next = m_lvl * m_lvl * 100;
	m_exp.left = m_exp.next - m_exp.current;

	m_energy.current = obj.value( "e" ).toVariant().toDouble();
	m_energy.max = obj.value( "em" ).toVariant().toDouble();

	m_hp.current = obj.value( "h" ).toVariant().toDouble();
	m_hp.max = obj.value( "hm" ).toVariant().toDouble();

	m_mood.current = obj.value( "m" ).toVariant().toDouble();
	m_mood.max = obj.value( "mm" ).toVariant().toDouble();

	for ( int i = 0; i < INVENTORY_SIZE; i++ )
		m_skills[ i ] = 0;

	obj = QJsonDocument::fromJson( obj.value( "data" ).toString().toUtf8() ).object().value( "ps" ).toObject();
	foreach ( QString key, obj.keys() )
		m_skills[ key.toInt() ] = obj.value( key ).toVariant().toDouble();

	data = QJsonDocument::fromJson( m_network->getData( "App.AccountsData=([^;]+);" ).toUtf8() );
	obj = data.object().value( QString::number( m_id ) ).toObject();

	m_money = obj.value( "money" ).toVariant().toDouble();

	int inventory[ INVENTORY_SIZE ] = { 0 };

	QJsonObject itemsObj = QJsonDocument::fromJson( obj.value( "items" ).toString().toUtf8() ).object();
	foreach ( QString key, itemsObj.keys() )
		inventory[ key.toInt() ] = itemsObj.value( key ).toVariant().toInt();

	m_inventory->updateInventory( inventory );

	emit somethingChanged();
}

void sAccount::updateFromAjax( const QString &json )
{
	QJsonDocument data = QJsonDocument::fromJson( json.toUtf8() );

	if ( data.object().value( "err" ).isString() )
	{
		m_log->warning( "simcracy returned: " + data.object().value( "err" ).toString() );
		return;
	}

	QJsonObject obj = data.object().value( "data" ).toObject().value( "auto" ).toObject();

	if ( obj.value( "user" ).isObject() )
	{
		QJsonObject objUser = obj.value( "user" ).toObject();

		if ( !objUser.value( "e" ).isUndefined() )
			m_energy.current = objUser.value( "e" ).toVariant().toDouble();
		if ( !objUser.value( "h" ).isUndefined() )
			m_hp.current = objUser.value( "h" ).toVariant().toDouble();
		if ( !objUser.value( "m" ).isUndefined() )
			m_mood.current = objUser.value( "m" ).toVariant().toDouble();
		if ( !objUser.value( "exp" ).isUndefined() )
		{
			m_exp.current = objUser.value( "exp" ).toVariant().toDouble();

			m_lvl = ( int )sqrt( m_exp.current / 100 ) + 1;

			m_exp.next = m_lvl * m_lvl * 100;
			m_exp.left = m_exp.next - m_exp.current;
		}
	}

	if ( obj.value( "account" ).isObject() )
	{
		QJsonObject objAccount = obj.value( "account" ).toObject();

		if ( !objAccount.value( "items" ).isUndefined() )
		{
			int inventory[ INVENTORY_SIZE ] = { 0 };

			QJsonObject itemsObj = objAccount.value( "items" ).toObject();
			foreach ( QString key, itemsObj.keys() )
				inventory[ key.toInt() ] = itemsObj.value( key ).toVariant().toInt();

			m_inventory->updateInventory( inventory );
		}
	}

	emit somethingChanged();
}
