/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#include <QtCore/QFile>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QJsonValue>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QPushButton>

#include "sBrowser.h"
#include "sControl.h"

sControl::sControl()
{
	ui.setupUi( this );
	ui.inventoryButton->setEnabled( false );
	ui.openBrowserButton->setEnabled( false );
	ui.runButton->setEnabled( false );

	m_trayIcon = new sTrayIcon( this );

	connect( ui.botsBox, static_cast< void ( QComboBox::* )( int )>( &QComboBox::currentIndexChanged ), [ & ]( int index )
		{
			refreshInfo( m_bots.at( index ) );
		}
	);

	connect( ui.inventoryButton, &QPushButton::clicked, [ & ]( int )
		{
		}
	);
	connect( ui.openBrowserButton, &QPushButton::clicked, [ & ]( int )
		{
			new sBrowser( m_bots.at( ui.botsBox->currentIndex() )->networkAccessManager() );
		}
	);
	connect( ui.runButton, &QPushButton::clicked, [ & ]( int )
		{
			m_bots.at( ui.botsBox->currentIndex() )->runScripts();
		}
	);

	load();
	show();
}

sControl::~sControl()
{
	foreach ( sBot *bot, m_bots )
	{
		bot->deleteLater();
	}

	m_trayIcon->deleteLater();
}

void sControl::load()
{
	QString fileName = QString( "%1/bots/bots.json" ).arg( QCoreApplication::applicationDirPath() );
	QFile botsFile( fileName );
	if ( !botsFile.open( QIODevice::ReadOnly | QIODevice::Text ) )
	{
		QMessageBox::critical( 0, "Error", QString( "The file %1 can't be opened for reading." ).arg( fileName ) );
		exit( 1 );
	}

	QJsonDocument botsJson = QJsonDocument::fromJson( botsFile.readAll() );
	botsFile.close();

	if ( !botsJson.isArray() )
	{
		QMessageBox::critical( 0, "Error", QString( "The file %1 has invalid format." ).arg( fileName ) );
		exit( 1 );
	}

	QJsonArray bots = botsJson.array();
	foreach ( QJsonValue value, bots )
	{
		QJsonObject botRecord = value.toObject();

		QString name = botRecord.value( "name" ).toString();
		QString path = botRecord.value( "path" ).toString();

		sBot *bot = new sBot( QString( "%1/bots/%2" ).arg( QCoreApplication::applicationDirPath() ).arg( path ) );
		m_bots.append( bot );

		connect( bot, &sBot::somethingChanged, [ & ]()
			{
				refreshInfo( m_bots.at( ui.botsBox->currentIndex() ) );
			}
		);

		ui.botsBox->addItem( name );
	}
}

void sControl::refreshInfo( sBot *bot )
{
	sBot::sBotStatus status = bot->status();
	ui.inventoryButton->setEnabled( status == sBot::STATUS_OK );
	ui.openBrowserButton->setEnabled( status == sBot::STATUS_OK );
	ui.runButton->setEnabled( status == sBot::STATUS_OK );

	ui.statusLabel->setText( bot->statusText() );

	ui.expLabel->setText( QString ( "%1 (%2)" ).arg( QString::number( bot->account()->lvl() ), QString::number( bot->account()->exp().current ) ) );
	ui.energyLabel->setText( QString::number( bot->account()->energy().current ) );
	ui.hpLabel->setText( QString::number( bot->account()->hp().current ) );
	ui.moodLabel->setText( QString::number( bot->account()->mood().current ) );
}
