/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#include <QtCore/QStringList>

#include "sInventory.h"

QStringList sInventory::m_names = QStringList() << "" << "grain" << "wood" << "iron_ore" << "iron" << "flour" << "bread" << "milk" << "cheese" << "oil" << "ticket" << "limestone" << "clay" << "sand" << "meat" << "sugarcane" << "sugar" << "fruits" << "vegetables" << "fish" << "titanium_ore" << "uranium_ore" << "uranium_235_1" << "uranium_235_20" << "uranium_235_99" << "meal" << "pizza" << "sushi" << "car" << "boat" << "gasoline" << "bullet" << "rocket" << "knife" << "gun" << "mp5" << "rocket_launcher" << "light_tank" << "mobile_RL" << "small_plane" << "paper" << "book" << "gold" << "cement" << "block" << "silicon_rod" << "microchip" << "rubber" << "tire" << "plastic" << "glass" << "stick" << "computer" << "medkit" << "coal" << "cake" << "chemicals" << "engine" << "electric_motor" << "titanium" << "bicycle" << "horse" << "tractor" << "tools" << "pill" << "rifle" << "buggy" << "apc";

sInventory::sInventory( sNetwork *network )
{
	m_network = network;
}

sInventory::~sInventory()
{
}

int sInventory::get( int id )
{
	if ( id < 1 || id >= INVENTORY_SIZE )
		return -1;

	return m_inventory[ id ];
}

int sInventory::getId( const QString &name )
{
	return m_names.indexOf( name );
}

QString sInventory::getName( int id )
{
	if ( id < 1 || id >= INVENTORY_SIZE )
		return -1;

	return m_names[ id ];
}

int sInventory::size()
{
	return INVENTORY_SIZE;
}

void sInventory::updateInventory( int *inventory )
{
	memcpy( m_inventory, inventory, INVENTORY_SIZE * sizeof( *inventory ) );
}
