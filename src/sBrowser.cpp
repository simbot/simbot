/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#include "sBrowser.h"

sBrowser::sBrowser( QNetworkAccessManager *networkAccessManager )
{
	ui.setupUi( this );
	setAttribute( Qt::WA_DeleteOnClose );

	m_webView = new QWebView( ui.layout->widget() );
	ui.layout->addWidget( m_webView );

	m_webView->page()->setNetworkAccessManager( networkAccessManager );

	connect( ui.goButton, &QPushButton::clicked, [ & ]( int )
		{
			load( ui.addressEdit->text() );
		}
	);
	connect( ui.addressEdit, &QLineEdit::returnPressed, [ & ]()
		{
			load( ui.addressEdit->text() );
		}
	);

	load( "http://simcracy.com" );

	showMaximized();
}

sBrowser::~sBrowser()
{
	m_webView->deleteLater();
}

void sBrowser::load( const QString &url )
{
	ui.addressEdit->setText( url );
	m_webView->load( url );
}
