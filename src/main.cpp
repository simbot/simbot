/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#include <QtCore/QTextCodec>
#include <QtWidgets/QApplication>

#include "sControl.h"

int main( int argc, char *argv[] )
{
	QTextCodec *codec = QTextCodec::codecForName( "UTF-8" );
	QTextCodec::setCodecForLocale( codec );

	QApplication a( argc, argv );

	sControl control;

	return a.exec();
}
