/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#ifndef SBROWSER_H
#define SBROWSER_H

#include <QtNetwork/QNetworkCookieJar>
#include <QtWebKit/QWebView>
#include <QtWidgets/QWidget>

#include "ui_sBrowser.h"

class sBrowser : public QWidget
{
	Q_OBJECT

public:
	sBrowser( QNetworkAccessManager *networkAccessManager );
	~sBrowser();

	void load( const QString &url );

private:
	Ui::sBrowserForm ui;

	QWebView *m_webView;
};

#endif // SBROWSER_H
