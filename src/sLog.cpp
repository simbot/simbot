/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#include <QtCore/QTextStream>

#include "sLog.h"

sLog::sLog( const QString &path )
{
	m_path = path;
	m_logFile = 0;

	openFile();
}

sLog::~sLog()
{
	closeFile();
}

void sLog::debug( const QString &message )
{
	write( "debug: ", message );
}

void sLog::information( const QString &message )
{
	write( QString(), message );
}

void sLog::warning( const QString &message )
{
	write( "warning: ", message );
}

void sLog::error( const QString &message )
{
	write( "ERROR: ", message );
}

void sLog::write( const QString &beginning, const QString &message )
{
	if ( QDate::currentDate() != m_date )
		openFile();

	if ( !m_logFile )
		return;

	QTime time = QTime::currentTime();
	QTextStream out( m_logFile );

	QString str;
	str.sprintf( "%02d:%02d:%02d %02d/%02d/%04d", time.hour(), time.minute(), time.second(), m_date.day(), m_date.month(), m_date.year() );

	out << str << ": " << beginning << message << endl;
}

void sLog::openFile()
{
	if ( m_logFile )
	{
		closeFile();
	}

	m_date = QDate::currentDate();
	QString date;
	date.sprintf( "%04d-%02d-%02d",  m_date.year(),  m_date.month(), m_date.day() );
	QString filePath = QString( "%1/logs/%2.log" ).arg( m_path ).arg( date );

	m_logFile = new QFile( filePath );
	m_logFile->open( QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text );

	if ( m_logFile->size() )
		m_logFile->write( "\n\n" );
}

void sLog::closeFile()
{
	if ( !m_logFile )
		return;

	m_logFile->close();
	m_logFile->deleteLater();
	m_logFile = 0;
}
