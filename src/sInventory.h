/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#ifndef SINVENTORY_H
#define SINVENTORY_H

#include <QtCore/QObject>

#include "sNetwork.h"

enum
{
	INVENTORY_SIZE = 67 + 1 // 0 isn't used
};

class sInventory : public QObject
{
	Q_OBJECT

	Q_PROPERTY( int size READ size )

	Q_PROPERTY( int grain READ grain )
	Q_PROPERTY( int wood READ wood )
	Q_PROPERTY( int iron_ore READ iron_ore )
	Q_PROPERTY( int iron READ iron )
	Q_PROPERTY( int flour READ flour )
	Q_PROPERTY( int bread READ bread )
	Q_PROPERTY( int milk READ milk )
	Q_PROPERTY( int cheese READ cheese )
	Q_PROPERTY( int oil READ oil )
	Q_PROPERTY( int ticket READ ticket )
	Q_PROPERTY( int limestone READ limestone )
	Q_PROPERTY( int clay READ clay )
	Q_PROPERTY( int sand READ sand )
	Q_PROPERTY( int meat READ meat )
	Q_PROPERTY( int sugarcane READ sugarcane )
	Q_PROPERTY( int sugar READ sugar )
	Q_PROPERTY( int fruits READ fruits )
	Q_PROPERTY( int vegetables READ vegetables )
	Q_PROPERTY( int fish READ fish )
	Q_PROPERTY( int titanium_ore READ titanium_ore )
	Q_PROPERTY( int uranium_ore READ uranium_ore )
	Q_PROPERTY( int uranium_235_1 READ uranium_235_1 )
	Q_PROPERTY( int uranium_235_20 READ uranium_235_20 )
	Q_PROPERTY( int uranium_235_99 READ uranium_235_99 )
	Q_PROPERTY( int meal READ meal )
	Q_PROPERTY( int pizza READ pizza )
	Q_PROPERTY( int sushi READ sushi )
	Q_PROPERTY( int car READ car )
	Q_PROPERTY( int boat READ boat )
	Q_PROPERTY( int gasoline READ gasoline )
	Q_PROPERTY( int bullet READ bullet )
	Q_PROPERTY( int rocket READ rocket )
	Q_PROPERTY( int knife READ knife )
	Q_PROPERTY( int gun READ gun )
	Q_PROPERTY( int mp5 READ mp5 )
	Q_PROPERTY( int rocket_launcher READ rocket_launcher )
	Q_PROPERTY( int light_tank READ light_tank )
	Q_PROPERTY( int mobile_RL READ mobile_RL )
	Q_PROPERTY( int small_plane READ small_plane )
	Q_PROPERTY( int paper READ paper )
	Q_PROPERTY( int book READ book )
	Q_PROPERTY( int gold READ gold )
	Q_PROPERTY( int cement READ cement )
	Q_PROPERTY( int block READ block )
	Q_PROPERTY( int silicon_rod READ silicon_rod )
	Q_PROPERTY( int microchip READ microchip )
	Q_PROPERTY( int rubber READ rubber )
	Q_PROPERTY( int tire READ tire )
	Q_PROPERTY( int plastic READ plastic )
	Q_PROPERTY( int glass READ glass )
	Q_PROPERTY( int stick READ stick )
	Q_PROPERTY( int computer READ computer )
	Q_PROPERTY( int medkit READ medkit )
	Q_PROPERTY( int coal READ coal )
	Q_PROPERTY( int cake READ cake )
	Q_PROPERTY( int chemicals READ chemicals )
	Q_PROPERTY( int engine READ engine )
	Q_PROPERTY( int electric_motor READ electric_motor )
	Q_PROPERTY( int titanium READ titanium )
	Q_PROPERTY( int bicycle READ bicycle )
	Q_PROPERTY( int horse READ horse )
	Q_PROPERTY( int tractor READ tractor )
	Q_PROPERTY( int tools READ tools )
	Q_PROPERTY( int pill READ pill )
	Q_PROPERTY( int rifle READ rifle )
	Q_PROPERTY( int buggy READ buggy )
	Q_PROPERTY( int apc READ apc )

public:
	sInventory( sNetwork *network );
	~sInventory();

	void updateInventory( int *inventory );

	Q_INVOKABLE int get( int id );

	Q_INVOKABLE int getId( const QString &name );
	Q_INVOKABLE QString getName( int id );

	Q_INVOKABLE int size();

	Q_INVOKABLE int grain() { return m_inventory[1]; }
	Q_INVOKABLE int wood() { return m_inventory[2]; }
	Q_INVOKABLE int iron_ore() { return m_inventory[3]; }
	Q_INVOKABLE int iron() { return m_inventory[4]; }
	Q_INVOKABLE int flour() { return m_inventory[5]; }
	Q_INVOKABLE int bread() { return m_inventory[6]; }
	Q_INVOKABLE int milk() { return m_inventory[7]; }
	Q_INVOKABLE int cheese() { return m_inventory[8]; }
	Q_INVOKABLE int oil() { return m_inventory[9]; }
	Q_INVOKABLE int ticket() { return m_inventory[10]; }
	Q_INVOKABLE int limestone() { return m_inventory[11]; }
	Q_INVOKABLE int clay() { return m_inventory[12]; }
	Q_INVOKABLE int sand() { return m_inventory[13]; }
	Q_INVOKABLE int meat() { return m_inventory[14]; }
	Q_INVOKABLE int sugarcane() { return m_inventory[15]; }
	Q_INVOKABLE int sugar() { return m_inventory[16]; }
	Q_INVOKABLE int fruits() { return m_inventory[17]; }
	Q_INVOKABLE int vegetables() { return m_inventory[18]; }
	Q_INVOKABLE int fish() { return m_inventory[19]; }
	Q_INVOKABLE int titanium_ore() { return m_inventory[20]; }
	Q_INVOKABLE int uranium_ore() { return m_inventory[21]; }
	Q_INVOKABLE int uranium_235_1() { return m_inventory[22]; }
	Q_INVOKABLE int uranium_235_20() { return m_inventory[23]; }
	Q_INVOKABLE int uranium_235_99() { return m_inventory[24]; }
	Q_INVOKABLE int meal() { return m_inventory[25]; }
	Q_INVOKABLE int pizza() { return m_inventory[26]; }
	Q_INVOKABLE int sushi() { return m_inventory[27]; }
	Q_INVOKABLE int car() { return m_inventory[28]; }
	Q_INVOKABLE int boat() { return m_inventory[29]; }
	Q_INVOKABLE int gasoline() { return m_inventory[30]; }
	Q_INVOKABLE int bullet() { return m_inventory[31]; }
	Q_INVOKABLE int rocket() { return m_inventory[32]; }
	Q_INVOKABLE int knife() { return m_inventory[33]; }
	Q_INVOKABLE int gun() { return m_inventory[34]; }
	Q_INVOKABLE int mp5() { return m_inventory[35]; }
	Q_INVOKABLE int rocket_launcher() { return m_inventory[36]; }
	Q_INVOKABLE int light_tank() { return m_inventory[37]; }
	Q_INVOKABLE int mobile_RL() { return m_inventory[38]; }
	Q_INVOKABLE int small_plane() { return m_inventory[39]; }
	Q_INVOKABLE int paper() { return m_inventory[40]; }
	Q_INVOKABLE int book() { return m_inventory[41]; }
	Q_INVOKABLE int gold() { return m_inventory[42]; }
	Q_INVOKABLE int cement() { return m_inventory[43]; }
	Q_INVOKABLE int block() { return m_inventory[44]; }
	Q_INVOKABLE int silicon_rod() { return m_inventory[45]; }
	Q_INVOKABLE int microchip() { return m_inventory[46]; }
	Q_INVOKABLE int rubber() { return m_inventory[47]; }
	Q_INVOKABLE int tire() { return m_inventory[48]; }
	Q_INVOKABLE int plastic() { return m_inventory[49]; }
	Q_INVOKABLE int glass() { return m_inventory[50]; }
	Q_INVOKABLE int stick() { return m_inventory[51]; }
	Q_INVOKABLE int computer() { return m_inventory[52]; }
	Q_INVOKABLE int medkit() { return m_inventory[53]; }
	Q_INVOKABLE int coal() { return m_inventory[54]; }
	Q_INVOKABLE int cake() { return m_inventory[55]; }
	Q_INVOKABLE int chemicals() { return m_inventory[56]; }
	Q_INVOKABLE int engine() { return m_inventory[57]; }
	Q_INVOKABLE int electric_motor() { return m_inventory[58]; }
	Q_INVOKABLE int titanium() { return m_inventory[59]; }
	Q_INVOKABLE int bicycle() { return m_inventory[60]; }
	Q_INVOKABLE int horse() { return m_inventory[61]; }
	Q_INVOKABLE int tractor() { return m_inventory[62]; }
	Q_INVOKABLE int tools() { return m_inventory[63]; }
	Q_INVOKABLE int pill() { return m_inventory[64]; }
	Q_INVOKABLE int rifle() { return m_inventory[65]; }
	Q_INVOKABLE int buggy() { return m_inventory[66]; }
	Q_INVOKABLE int apc() { return m_inventory[67]; }
	
private:
	sNetwork *m_network;

	static QStringList m_names;

	int m_inventory[ INVENTORY_SIZE ];
};

#endif // SINVENTORY_H
