/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#include "sControl.h"
#include "sTrayIcon.h"

sTrayIcon::sTrayIcon( sControl *control )
{
	m_control = control;

	connect( this, SIGNAL( activated( QSystemTrayIcon::ActivationReason ) ), this, SLOT( activated( QSystemTrayIcon::ActivationReason ) ) );

	show();
}

sTrayIcon::~sTrayIcon()
{
	hide();
}

void sTrayIcon::activated( QSystemTrayIcon::ActivationReason r )
{
	if ( r == QSystemTrayIcon::DoubleClick )
	{
		if ( m_control->isVisible() )
			m_control->hide();
		else
			m_control->show();
	}
}
