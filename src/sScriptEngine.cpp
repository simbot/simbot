/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#include <QtCore/QFile>
#include <QtScript/QtScript>

#include "sTypes.h"
#include "sScriptEngine.h"

sScriptEngine::sScriptEngine( const QString &path )
{
	qScriptRegisterMetaType< exp_t >( this, sTypes::toScriptValue, sTypes::fromScriptValue );
	qScriptRegisterMetaType< cm_pair_t >( this, sTypes::toScriptValue, sTypes::fromScriptValue );

	setProperty( "path", path );

	QScriptValue includeFunction = newFunction( &sScriptEngine::include );
	globalObject().setProperty( "include", includeFunction );
}

sScriptEngine::~sScriptEngine()
{
}

void sScriptEngine::addProperty( void *object, const QString &name )
{
	QScriptValue value = newQObject( reinterpret_cast< QObject * >( object ) );
	globalObject().setProperty( name, value );
}

QScriptValue sScriptEngine::include( QScriptContext *context, QScriptEngine *engine )
{
	if ( engine == 0 )
		return QScriptValue( false );

	QString fileName = context->argument ( 0 ).toString();
	QString filePath = engine->property( "path" ).toString() + fileName;
	QString contents;

	QFile file( filePath );
	if ( file.open( QFile::ReadOnly ) )
	{
		contents = file.readAll();
		file.close();
	}
	else
	{
		filePath = QCoreApplication::applicationDirPath() + "/scripts/" + fileName;
		file.setFileName( filePath );

		if ( file.open( QFile::ReadOnly ) )
		{
			contents = file.readAll();
			file.close();
		}
		else
		{
			engine->globalObject().property( "log" ).property( "error" ).call( context->thisObject(), QScriptValueList() << QString( "include: can't open %1 to read" ).arg( filePath ) );
			return QScriptValue( false );
		}
	}

	engine->currentContext()->setActivationObject( engine->currentContext()->parentContext()->activationObject() );
	return engine->evaluate( contents, fileName );
}
