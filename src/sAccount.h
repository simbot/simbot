/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#ifndef SACCOUNT_H
#define SACCOUNT_H

#include <QtCore/QObject>

#include "sBuilding.h"
#include "sInventory.h"
#include "sLog.h"
#include "sNetwork.h"
#include "sTypes.h"

class sAccount : public QObject
{
	Q_OBJECT

	Q_PROPERTY( int id READ id )
	Q_PROPERTY( int x READ x )
	Q_PROPERTY( int y READ y )

	Q_PROPERTY( int lvl READ lvl )

	Q_PROPERTY( exp_t exp READ exp )
	Q_PROPERTY( cm_pair_t energy READ energy )
	Q_PROPERTY( cm_pair_t hp READ hp )
	Q_PROPERTY( cm_pair_t mood READ mood )

	Q_PROPERTY( double money READ money )

public:
	sAccount( sLog *log, sNetwork *network );
	~sAccount();

	int id();
	int x();
	int y();

	int lvl();
	Q_INVOKABLE double skill( int id );

	exp_t exp();
	cm_pair_t energy();
	cm_pair_t hp();
	cm_pair_t mood();
	
	double money();

	sBuilding *building();
	sInventory *inventory();

	Q_INVOKABLE void consume( int id, int count );
	Q_INVOKABLE void sendMessage( int id, QString message );
	Q_INVOKABLE void sendItems( int id, double money, QVariantList items );

	void updateFromAjax( const QString &json );
	void updateInformation();

signals:
	void somethingChanged();

private:
	sLog *m_log;
	sBuilding *m_building;
	sInventory *m_inventory;
	sNetwork *m_network;

	int m_id;
	int m_x;
	int m_y;

	int m_lvl;

	exp_t m_exp;
	cm_pair_t m_energy;
	cm_pair_t m_hp;
	cm_pair_t m_mood;

	double m_money;
	double m_skills[ INVENTORY_SIZE ];
};

#endif // SACCOUNT_H
