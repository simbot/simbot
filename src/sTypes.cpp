/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#include <QtScript/QScriptEngine>
#include <QtScript/QScriptValue>

#include "sTypes.h"

// exp_t
QScriptValue sTypes::toScriptValue( QScriptEngine *engine, const exp_t &s )
{
	QScriptValue obj = engine->newObject();

	obj.setProperty( "current", s.current );
	obj.setProperty( "left", s.left );
	obj.setProperty( "next", s.next );

	return obj;
}

void sTypes::fromScriptValue( const QScriptValue &obj, exp_t &s )
{
	s.current = obj.property( "current" ).toVariant().toDouble();
	s.left = obj.property( "left" ).toVariant().toDouble();
	s.next = obj.property( "next" ).toVariant().toDouble();
}

// cm_pair_t
QScriptValue sTypes::toScriptValue( QScriptEngine *engine, const cm_pair_t &s )
{
	QScriptValue obj = engine->newObject();

	obj.setProperty( "current", s.current );
	obj.setProperty( "max", s.max );

	return obj;
}

void sTypes::fromScriptValue( const QScriptValue &obj, cm_pair_t &s )
{
	s.current = obj.property( "current" ).toVariant().toDouble();
	s.max = obj.property( "max" ).toVariant().toDouble();
}
