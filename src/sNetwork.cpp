/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#include <QtCore/QRegExp>
#include <QtNetwork/QtNetwork>

#include "sNetwork.h"

sNetwork::sNetwork( const QString &email, const QString &password, int type, sLog *log )
{
	m_email = email;
	m_password = password;
	m_type = type;

	m_log = log;
	
	m_manager = new QNetworkAccessManager( this );
	connect( m_manager, &QNetworkAccessManager::sslErrors, [] ( QNetworkReply *reply, const QList< QSslError > &errors )
		{
			reply->ignoreSslErrors();
		}
	);

	m_log->information( "network module loaded" );

	m_status = STATUS_NOTAUTHED;

	auth();
	if ( isAuthorised() )
	{
		m_status = STATUS_AUTHED;
		m_log->information( "login successful" );
	}
	else
	{
		m_status = STATUS_NOTAUTHED;
		m_log->error( "login failed" );
	}
}

sNetwork::~sNetwork()
{
}

QNetworkReply *sNetwork::request( const QString &url, int type, QByteArray &data )
{
	return synReq( QNetworkRequest( url ), type, data );
}

QString sNetwork::getData( const QString &data, const QString &pattern )
{
	QRegExp rx( pattern );
	rx.indexIn( data );

	return rx.cap( 1 );
}

QString sNetwork::getData( const QString &pattern )
{
	QNetworkRequest req( QUrl( "http://simcracy.com/messages" ) );

	QNetworkReply *reply = synReq( req );
	QString body( reply->readAll() );
	reply->deleteLater();

	QRegExp rx( pattern );
	rx.indexIn( body );

	return rx.cap( 1 );
}

QNetworkAccessManager *sNetwork::networkAccessManager()
{
	return m_manager;
}

sNetwork::sNetworkStatus sNetwork::status()
{
	return m_status;
}

QNetworkReply *sNetwork::synReq( QNetworkRequest &req, int type, QByteArray &data )
{
	QNetworkReply *reply;
	if ( type == RQ_GET )
		reply = m_manager->get( req );
	else
	{
		if ( type == RQ_AJAX )
			data.prepend( QString( "_token=%1&" ).arg( m_token ).toUtf8() );

		req.setHeader( QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded" );
		reply = m_manager->post( req, data );
	}

	QEventLoop eventLoop;
	connect( reply, &QNetworkReply::finished, &eventLoop, &QEventLoop::quit );
	eventLoop.exec();

	QUrl redirect = reply->attribute( QNetworkRequest::RedirectionTargetAttribute ).toUrl();
	if ( !redirect.isEmpty() )
	{
		reply->deleteLater();

		QNetworkRequest nreq( redirect );
		return synReq( nreq );
	}

	return reply;
}

void sNetwork::auth()
{
	if ( isAuthorised() )
		return;

	QString addr = "http://simcracy.com/signup/";
	if ( m_type == 0 )
		addr += "facebook";
	else
		addr += "google";

	QNetworkReply *reply = synReq( QNetworkRequest( addr ) );
	QString body = reply->readAll();
	reply->deleteLater();

	if ( m_type == 0 )
	{
		QRegExp urlRx( "<form id=\"login_form\" action=\"([^\"]+)\"" );
		urlRx.indexIn( body );
		QString url = urlRx.cap( 1 ).replace( "&amp;", "&" );

		QNetworkRequest req( url );

		QByteArray data;
		data.append( QString( "email=%1&" ).arg( m_email ) );
		data.append( QString( "pass=%1" ).arg( m_password ) );

		reply = synReq( req, RQ_POST, data );
		reply->deleteLater();
	}
	else
	{
		QRegExp continueRx( "id=\"continue\"\\s+value=\"([^\"]+)\"" );
		continueRx.indexIn( body );
		QString continueUrl = QUrl::toPercentEncoding( continueRx.cap( 1 ).replace( "&amp;", "&" ) );
		
		QRegExp galxRx( "name=\"GALX\"\\s+value=\"([^\"]+)\"" );
		galxRx.indexIn( body );
		QString galx = galxRx.cap( 1 );

		QRegExp dshRx( "id=\"dsh\"\\s+value=\"([^\"]+)\"" );
		dshRx.indexIn( body );
		QString dsh = dshRx.cap( 1 );

		QNetworkRequest req( QUrl( "https://accounts.google.com/ServiceLoginAuth" ) );

		QByteArray data;
		data.append( QString( "Email=%1&" ).arg( m_email ) );
		data.append( QString( "Passwd=%1&" ).arg( m_password ) );
		data.append( QString( "continue=%1&" ).arg( continueUrl ) );
		data.append( QString( "GALX=%1&" ).arg( galx ) );
		data.append( QString( "dsh=%1" ).arg( dsh ) );

		reply = synReq( req, RQ_POST, data );
		reply->deleteLater();
	}
}

bool sNetwork::isAuthorised()
{
	QString accountData = getData( "App.AccountsData=([^;]+);" );

	if ( accountData == "0" )
		return 0;

	m_token = getData( "_token='([^']+)';" );

	return 1;
}
