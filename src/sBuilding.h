/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#ifndef SBUILDING_H
#define SBUILDING_H

#include <QtCore/QObject>

#include "sNetwork.h"
#include "sTypes.h"

class sAccount;

class sBuilding : public QObject
{
	Q_OBJECT

public:
	sBuilding( sAccount *account, sNetwork *network );
	~sBuilding();

	Q_INVOKABLE binfo_t info( int id, int x, int y );

	Q_INVOKABLE void work( int id, int count );
	Q_INVOKABLE void workMax( int id, int x, int y );
	
private:
	sAccount *m_account;
	sNetwork *m_network;
};

#endif // SBUILDING_H
