/****************************************************************************
 *  simbot
 *
 *  Copyright (c) 2012 by Belov Nikita <zodiac.nv@gmail.com>
 *
 ***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************
*****************************************************************************/

#ifndef SBOT_H
#define SBOT_H

#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QtNetwork/QNetworkCookieJar>
#include <QtScript/QtScript>

#include "sAccount.h"
#include "sLog.h"
#include "sNetwork.h"
#include "sScriptEngine.h"

class sBot : public QObject
{
	Q_OBJECT

public:
	enum sBotStatus
	{
		STATUS_UNKNOWN,
		STATUS_LOADING,
		STATUS_LOADED,
		STATUS_AUTHED,
		STATUS_OK,
		STATUS_LOAD_FAILED,
		STATUS_AUTH_FAILED
	};

	sBot( const QString &path );
	~sBot();

	sBotStatus status();
	QString statusText();

	QNetworkAccessManager *networkAccessManager();

	sAccount *account();

public slots:
	void runScripts();

signals:
	void somethingChanged();

private:
	int loadSettings();
	void initScriptEngine();

	sBotStatus m_status;

	QTimer *m_timer;
	sAccount *m_account;
	sLog *m_log;
	sNetwork *m_network;
	sScriptEngine *m_script;

	QString m_path;
	QStringList m_scripts;
};

#endif // SBOT_H
